import numpy as np
from random import shuffle

def svm_loss_naive(W, X, y, reg):
  """
  Structured SVM loss function, naive implementation (with loops).

  Inputs have dimension D, there are C classes, and we operate on minibatches
  of N examples.

  Inputs:
  - W: A numpy array of shape (D, C) containing weights.
  - X: A numpy array of shape (N, D) containing a minibatch of data.
  - y: A numpy array of shape (N,) containing training labels; y[i] = c means
    that X[i] has label c, where 0 <= c < C.
  - reg: (float) regularization strength

  Returns a tuple of:
  - loss as single float
  - gradient with respect to weights W; an array of same shape as W
  """
  dW = np.zeros(W.shape) # initialize the gradient as zero

  # compute the loss and the gradient
  num_classes = W.shape[1]
  num_train = X.shape[0]
  loss = 0.0

  for i in range(num_train):
    scores = X[i].dot(W)
    correct_class_score = scores[y[i]]
    for j in range(num_classes):
      if j == y[i]:
        continue
      margin = scores[j] - correct_class_score + 1 # note delta = 1
      if margin > 0:
        dmargin_dW = np.zeros(W.shape)
        dmargin_dW[:,j] = X[i].transpose()
        dmargin_dW[:,y[i]] = -X[i].transpose()
        loss += margin
        dW += dmargin_dW
  # Right now the loss is a sum over all training examples, but we want it
  # to be an average instead so we divide by num_train.
  loss /= num_train
  dW /= num_train
  # Add regularization to the loss.
  loss += reg * np.sum(W * W)
  dW += 2*reg*W
  #############################################################################
  # TODO:                                                                     #
  # Compute the gradient of the loss function and store it dW.                #
  # Rather that first computing the loss and then computing the derivative,   #
  # it may be simpler to compute the derivative at the same time that the     #
  # loss is being computed. As a result you may need to modify some of the    #
  # code above to compute the gradient.                                       #
  #############################################################################


  return loss, dW


def svm_loss_vectorized(W, X, y, reg):
  """
  Structured SVM loss function, vectorized implementation.

  Inputs and outputs are the same as svm_loss_naive.
  """
  loss = 0.0
  dW = np.zeros(W.shape) # initialize the gradient as zero

  #############################################################################
  # TODO:                                                                     #
  # Implement a vectorized version of the structured SVM loss, storing the    #
  # result in loss.                                                           #
  #############################################################################
  num_classes = W.shape[1]
  num_train = X.shape[0]
  score_matrix = X.dot(W) #(N,C)

  #mask = np.zeros(score_matrix.shape)
  #mask[np.arange(num_train), y] = 1.
  #correct_class_score_vector = np.expand_dims(np.sum(score_matrix*mask, axis=1),axis=1)
  #score_matrix -= correct_class_score_vector - 1
  score_matrix -= np.expand_dims(score_matrix[np.arange(num_train), y],axis=1) - 1
  #mask = 1 - mask
  #score_matrix *= mask
  score_matrix[np.arange(num_train), y] = 0
  np.maximum(score_matrix, 0,score_matrix)
  #score_matrix[score_matrix<0] = 0

  loss += np.mean(np.sum(score_matrix, axis=1)) + reg * np.sum(W * W)

  #############################################################################
  #                             END OF YOUR CODE                              #
  #############################################################################


  #############################################################################
  # TODO:                                                                     #
  # Implement a vectorized version of the gradient for the structured SVM     #
  # loss, storing the result in dW.                                           #
  #                                                                           #
  # Hint: Instead of computing the gradient from scratch, it may be easier    #
  # to reuse some of the intermediate values that you used to compute the     #
  # loss.                                                                     #
  #############################################################################
  # d one output number / d matrix = result with shape of matrix
  mask = np.ones(score_matrix.shape)#(N,C)
  mask[score_matrix==0] = 0. # including the negtive score and label score
  num_non_zeros_mask = np.sum(mask, axis=1) # number of zeros in the mask row(except the label column)

  mask[np.arange(mask.shape[0]), y] = -num_non_zeros_mask
  
  dW += X.transpose().dot(mask)/num_train + 2*reg*W
  #dW += np.column_stack([np.mean((X *  np.expand_dims(mask[:,i], axis=1)), axis=0) for i in range(mask.shape[1])])
  #dW += np.mean((np.expand_dims(X, axis=2) *  np.expand_dims(mask, axis=1)), axis=0)
  #############################################################################
  #                             END OF YOUR CODE                              #
  #############################################################################

  return loss, dW
