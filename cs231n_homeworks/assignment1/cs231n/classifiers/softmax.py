import numpy as np
from random import shuffle

def softmax_loss_naive(W, X, y, reg):
  """
  Softmax loss function, naive implementation (with loops)

  Inputs have dimension D, there are C classes, and we operate on minibatches
  of N examples.

  Inputs:
  - W: A numpy array of shape (D, C) containing weights.
  - X: A numpy array of shape (N, D) containing a minibatch of data.
  - y: A numpy array of shape (N,) containing training labels; y[i] = c means
    that X[i] has label c, where 0 <= c < C.
  - reg: (float) regularization strength

  Returns a tuple of:
  - loss as single float
  - gradient with respect to weights W; an array of same shape as W
  """
  # Initialize the loss and gradient to zero.
  loss = 0.0
  dW = np.zeros_like(W)

  #############################################################################
  # TODO: Compute the softmax loss and its gradient using explicit loops.     #
  # Store the loss in loss and the gradient in dW. If you are not careful     #
  # here, it is easy to run into numeric instability. Don't forget the        #
  # regularization!                                                           #
  #############################################################################
  # https://www.ics.uci.edu/~pjsadows/notes.pdf
  num_classes = W.shape[1]
  num_train = X.shape[0]
  epsilon = 1e-9
  for n in range(num_train):
    scores = X[n].dot(W)
    scores_max = np.amax(scores)
    z = scores-scores_max
    #scores_exp = np.exp(z)

    #softmax_right = scores_exp[y[n]] / score_exp_sum # may be divided by 0, turn the / to -
    log_softmax = z - np.log(np.sum(np.exp(z))) # log(exp(z[y])/sum(exp(z)))
    
    loss += - log_softmax[y[n]]#np.log(softmax_right)#+epsilon

    for c in range(num_classes):
      dW[:,c] += (np.exp(log_softmax[c]) - (c==y[n])) * X[n]      
      #softmax_temp = scores_exp[c] / score_exp_sum
      # here loss is not been added together yet, we only calculate dsoftmax_c/dz_k, and have to decide whether c == k
      #dW[:,scores_max_index] += -(softmax_temp - (c==y[n])) * X[n]#can have or not, softmax_temp =1, whole=0
      


  loss /= num_train
  dW /= num_train
  loss += reg * np.sum(W * W)
  dW += 2*reg * W
  #############################################################################
  #                          END OF YOUR CODE                                 #
  #############################################################################

  return loss, dW


def softmax_loss_vectorized(W, X, y, reg):
  """
  Softmax loss function, vectorized version.

  Inputs and outputs are the same as softmax_loss_naive.
  """
  # Initialize the loss and gradient to zero.
  loss = 0.0
  dW = np.zeros_like(W)

  #############################################################################
  # TODO: Compute the softmax loss and its gradient using no explicit loops.  #
  # Store the loss in loss and the gradienThe Good Fightt in dW. If you are not careful     #
  # here, it is easy to run into numeric instability. Don't forget the        #
  # regularization!                                                           #
  #############################################################################
  #num_classes = W.shape[1]
  num_train = X.shape[0]
  product = X.dot(W)
  epsilon = 1e-9
  maximum = np.amax(product, axis=1, keepdims=True) #(N,)
  #exp_product = np.exp(product-maximum) #(N,C)
  Z = product - maximum

  log_softmax = Z - np.log(np.sum(np.exp(Z), axis=1, keepdims=True)) #(N,)
  loss += -np.mean(log_softmax[np.arange(num_train),y]) + reg * np.sum(W * W) #log(+epsilon)
  
  mask = np.zeros_like(product)
  mask[range(num_train),y] = 1.
  dW += X.transpose().dot((np.exp(log_softmax)-mask))/num_train + 2*reg * W

  #############################################################################
  #                          END OF YOUR CODE                                 #
  #############################################################################

  return loss, dW

